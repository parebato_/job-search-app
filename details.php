<?php include_once 'config/init.php';  ?>

<?php 

$job = new Job;
 
//delete a job

if(isset($_POST['del_id'])){
	//create a var for delete
	$del_id = $_POST['del_id'];

	if($job->delete($del_id)){
		redirect('index.php', 'You just deleted a job post', 'success');
	}else{
		redirect('index.php', 'Opps! something went wrong. Please try again.', 'error');
	}
}

 $template = new Template('templates/job-details.php');
 
 $jobId = isset($_GET['id']) ? $_GET['id'] : null;

 $template->job= $job->getJobDetails($jobId);

 echo $template;
