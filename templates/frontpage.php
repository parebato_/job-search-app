<?php include 'includes/header.php'; ?>

<div class="container">
<main role="main">

  <!-- Main jumbotron for a primary marketing message or call to action -->
  <div class="jumbotron">
    <div class="container">
      <h1>Search Jobs</h1>
       <form action="index.php" method="GET">
         <select name="category" class="form-control">
           <option value="0">Select Category</option>

           <?php foreach($categories as $category): ?>

            <option value="<?php echo $category->id; ?>"><?php echo $category->name; ?></option>

           <?php endforeach; ?>
         </select>
         <input value="Search" type="submit" class="btn btn-dark my-4">

       </form>
     
    </div>
  </div>

  <div class="container">
   
   <h3><?php echo $title ?></h3>

    <?php 
      foreach ($jobs as $job):
     ?>
    <div class="row align-items-center justify-content-center">
      <div class="col-md-9">

        <h4><?php echo $job->job_title; ?></h4>
        <p><?php echo $job->description; ?></p>
        <p>Salary range: <?php echo $job->salary; ?></p>
        <small >Category: <?php echo $job->cname; ?></small>
         <br>
        <small><?php echo $job->post_date; ?></small> 

      </div>

      <div class="col-md-3">
      	<p>
          <a class="btn btn-dark" 
          href="details.php?id=<?php echo $job->id;?>" 
          role="button">View details</a>
        </p>   
      </div>

    </div>

    <hr>

  <?php endforeach; ?>

  </div> <!-- /container -->

</main>

<!-- pagination -->



<?php include 'includes/footer.php'; ?>