<?php include 'includes/header.php'; ?>

<div>
	<h4 class="page-header">Post New Job</h4>

	<form method="POST" action="create.php" >
		<div class="form-group">
			<label>Company</label>
			<input type="text" name="company" class="form-control">
		</div>

		<div class="form-group">
			<label>Job Title</label>
			<input type="text" name="job_title" class="form-control">
		</div>

		<div class="form-group">
			<label>Category</label>
			<select name="category" class="form-control">
				<option value="">Select Category</option>
				<?php foreach ($categories as $category):?>
					<option value="<?php echo $category->id; ?>"><?php echo $category->name ?></option>
				<?php endforeach; ?>
			</select>
		</div>

		<div class="form-group">
			<label>Location</label>
			<input type="text" name="location" class="form-control">
		</div>

		<div class="form-group">
			<label>Description</label>
			<textarea type="text" name="description" class="form-control"></textarea> 
		</div>
		<div class="form-group">
			<label>Salary</label>
			<input type="text" name="salary" class="form-control">
		</div>

		<div class="form-group">
			<label>Contact User</label>
			<input type="text" name="contact_user" class="form-control">
		</div>

		<div class="form-group">
			<label>Contact Email</label>
			<input type="email" name="contact_email" class="form-control">
		</div>

		<input class="btn btn-dark" type="submit" value="Submit" name="submit">
	</form>
</div>



	<?php include 'includes/footer.php'; ?>

