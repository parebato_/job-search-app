<?php include_once 'config/init.php'; ?>

<?php
$job = new Job;

$jobId = isset($_GET['id']) ? $_GET['id'] : null;
//passing data to the database when button submit is clicked.
if(isset($_POST['submit'])){
	//Create Data Array
	$data = array();
	$data['job_title'] = $_POST['job_title'];
	$data['company'] = $_POST['company'];
	$data['category_id'] = $_POST['category'];
	$data['description'] = $_POST['description'];
	$data['location'] = $_POST['location'];
	$data['salary'] = $_POST['salary'];
	$data['contact_user'] = $_POST['contact_user'];
	$data['contact_email'] = $_POST['contact_email'];

	if($job->update($jobId, $data)){
		redirect('index.php', 'Job details updated', 'success');
	} else {
		redirect('index.php', 'Something went wrong', 'error');
	}
}

$template = new Template('templates/edit-job.php');

$template->categories = $job->getCategories();

$template->job= $job->getJobDetails($jobId);

echo $template;